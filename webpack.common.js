const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { InjectManifest } = require('workbox-webpack-plugin');
const port = process.env.PORT || 3000;

module.exports = {
    entry: { main: './src/index.js' },
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve('dist'),
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            "react-dom": "@hot-loader/react-dom",
        },
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localsConvention: 'camelCase',
                            sourceMap: true
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
        new HtmlWebpackPlugin({
            template: 'index.html'
        }),

        new webpack.HotModuleReplacementPlugin(),
        new InjectManifest({
            swSrc: './src/sw.js',
            maximumFileSizeToCacheInBytes: 5000000
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: "public", to: "." }
            ],
        })
    ],
    optimization: {
        splitChunks: {
            chunks: 'all',
        }
    }
};