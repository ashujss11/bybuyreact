import axios from "axios";
export default class DataProviderService {
  constructor() {
    this.accessToken = JSON.parse(
      sessionStorage.getItem("sellerDetail")
    ).authToken;
  }
  getHandler(handlerUrl, options) {
    var paramsData = {
      headers: { Authorization: this.accessToken },
      params: options
    };
    return axios.get(handlerUrl, paramsData);
  }
  postHandler(handlerUrl, body, flag) {
    var options = {};
    if (flag) {
      options = {
        headers: { Authorization: this.accessToken }
      };
    }
    return axios.post(handlerUrl, body, options);
  }
  putHandler(handlerUrl, body, flag) {
    var options = {};
    if (flag) {
      options = {
        headers: { Authorization: this.accessToken }
      };
    }
    return axios.put(handlerUrl, body, options);
  }
}
