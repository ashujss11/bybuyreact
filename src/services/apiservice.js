// import { Injectable } from '@angular/core';
// import { Observable, of } from 'rxjs';
// import { map } from 'rxjs/operators';
// import {DataProviderService} from './dataprovider'

export default class ApiService {
                   // 15.207.132.39:8089 https://tohands.in/
                   // baseUrl="http://15.207.132.39:8089"
                   baseUrl = 'https://tohands.in';

                   constructor(dataProviderService) {
                       this.dataProviderService = dataProviderService;
                   }

                   getproductData(requestParams) {
                       const url = `${this.baseUrl}/product/seller/getallproducts`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getSearchedproductData(name, requestParams) {
                       const url = `${this.baseUrl}/product/search`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getShopDetail(name, requestParams) {
                       const url = `${this.baseUrl}/seller/getInfo`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getSubcription(name, requestParams) {
                       const url = `${this.baseUrl}/seller/isSubscribe`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getProDetail(requestParams) {
                       const url = `${this.baseUrl}/product/${requestParams.prodId}`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getOfferDetail(name, requestParams) {
                       const url = `${this.baseUrl}/offer/getAllByLatLongAndCategory`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getSimilarProd(requestParams) {
                       const url = `${this.baseUrl}/product/getSimilarProducts`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getLogin(requestParams) {
                       const url = `${this.baseUrl}/buyer/register/login?username=${
                           requestParams.username
                       }${requestParams.otp ? `&otp=${requestParams.otp}` : ``}`;
                       return this.dataProviderService.postHandler(url, requestParams, false);
                   }

                   getBuyerinfo(name, requestParams) {
                       const url = `${this.baseUrl}/buyer/getInfo`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   saveSubscribe(requestParams) {
                       const url = `$ {this.baseUrl}/seller/subscribe?buyerId=${requestParams.buyerId}&sellerId=${requestParams.sellerId}`;
                       return this.dataProviderService.postHandler(url, requestParams, true);
                   }

                   saveUnSubscribe(requestParams) {
                       const url = `${this.baseUrl}/seller/unsubscribe?buyerId=${requestParams.buyerId}&sellerId=${requestParams.sellerId}`;
                       return this.dataProviderService.postHandler(url, requestParams, true);
                   }

                   getCategories(requestParams) {
                       const url = `${this.baseUrl}/seller/category`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getBuyerInfo(requestParams) {
                       const url = `${this.baseUrl}/buyer/getInfo`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   updateBuyerInfo(name, body) {
                       const url = `${this.baseUrl}/buyer/update`;
                       return this.dataProviderService.putHandler(url, body, true);
                   }

                   getStoreOffer(requestParams) {
                       const url = `${this.baseUrl}/offer/list`;
                       return this.dataProviderService.getHandler(url, requestParams);
                   }

                   getFinalItem(requestParams) {
                       const url = `${this.baseUrl}/cart/getCartBySeller/${requestParams.buyerId}/${requestParams.sellerId}/${requestParams.deliveryAddressId};`;
                       return this.dataProviderService.getHandler(url, {});
                   }

                   saveAddress(requestParams) {
                       const url = `${this.baseUrl}/buyer/address/add`;
                       return this.dataProviderService.postHandler(url, requestParams, true);
                   }

                   saveCartItem(requestParams) {
                       const url = `${this.baseUrl}/cart/product/add?productId=${requestParams.productId}&userId=${requestParams.userId}&count=${requestParams.count}`;
                       return this.dataProviderService.postHandler(url, {}, true);
                   }

                   updateCartItem(name, requestParams) {
                       const url = `${this.baseUrl}/cart/product/updateCount?productId=${requestParams.productId}&userId=${requestParams.userId}&count=${requestParams.count}`;
                       return this.dataProviderService.postHandler(url, {}, true);
                   }

                   removeCartItem(name, requestParams) {
                       const url = `${this.baseUrl}/cart/removeFromCart?productId=${requestParams.productId}&userId=${requestParams.userId}`;
                       return this.dataProviderService.putHandler(url, {}, true);
                   }

                   getGenerateOrder(requestParams) {
                       const url = `${this.baseUrl}/order/generateOrder/${requestParams.buyerId}/${requestParams.sellerId}/${requestParams.deliveryAddressId}/${requestParams.paymentMode}`;
                       return this.dataProviderService.getHandler(url, {});
                   }

                   getOrderStatus(requestParams) {
                       const url = `${this.baseUrl}/order/${requestParams.orderId}/transactionStatus/${requestParams.paymentMode}`;
                       let body = {};
                       if (requestParams.cashfree) {
                           body = { cashFreeResponseModel: requestParams.cashfree };
                       }
                       return this.dataProviderService.putHandler(url, body, true);
                   }

                   getCashOrderStatus(requestParams) {
                       const url = `${this.baseUrl}/order/status/${requestParams.orderId}`;
                       return this.dataProviderService.getHandler(url, {});
                   }
               }
