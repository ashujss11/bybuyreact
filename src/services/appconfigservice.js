import * as CryptoJS from 'crypto-js';
import Cookies from 'js-cookie';
import ApiService from './apiservice';
import DataProviderService from './dataprovider';
// import { Injectable } from '@angular/core';
// import { CookieService } from 'ngx-cookie-service';

// import { ApiService } from './services/api.service';
// import { StoreService } from './store.service';
// import { Subject } from 'rxjs';
// import { Observable } from 'rxjs/internal/Observable';

export default class AppConfigService {
    static apiService = null;
    constructor() {
        this.initialise();
        this.apiService = new ApiService(new DataProviderService());
        AppConfigService.apiService = this.apiService;

        // this.storeServices=StoreService;
    }

    static getAPIService() {
        return AppConfigService.apiService;
    }

    getApiService() {
        return this.apiService;
    }

    initialise() {
        localStorage.clear();
        /* Cookies.set(
                           'accessToken',
                           'nz6ZJXvuanTGtUXG7GgZO3e/fTGN8EJsnmq/N5lrGjG/CYkNPlL+ZbHDhSpg8opN+eMY9FHDU3CE4ah4aH8ADXoNHZe0+dweDZKRXEwaUGQAiInpr6/8cg28KIqsuJTaNqfRNO6XPNqbx8gNwQrZeQCQKcvpyq69hOLHanynChq4Ij9n1B/S2G7C2XN8Nx3PwlBlmPbPfcJ4KyjezBdMGKYt0ZQmflq2p3sK0vOpu4DVmEBKodUOt+8ug3YA417a9ZpoNN1oIAXgtGmg6eik7Tj39CBmKzKPiSjQOpXGKKslYA9NDZa4erY/wG97CsisY46gapRjKY3Gt945JCJzMQ=='
                       );*/
        const parsedBase64Key = CryptoJS.enc.Base64.parse('VGhlQmVzdFNlY3JldEtleQ==');
        const encryptedCipherText = this.replaceAll(Cookies.get('accessToken'), 'PLUS', '+');
        if (!encryptedCipherText) {
            window.location.href = 'https://www.bybuy.in/';
        }
        const decryptedData = CryptoJS.AES.decrypt(encryptedCipherText, parsedBase64Key, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        const decryptedText = decryptedData.toString(CryptoJS.enc.Utf8);
        sessionStorage.setItem('sellerDetail', decryptedText);
    }

    async getSomeData() {
        const requestParams = {
            mobileno: JSON.parse(sessionStorage.getItem('sellerDetail')).sellerMobileNo
        };
        // 8318797581 for prod JSON.parse(sessionStorage.getItem("sellerDetail")).sellerMobileNo
        let res = '';
        try {
            res = await this.apiService.getShopDetail(name, requestParams);
            res = res.data;

            if (res.code === '200') {
                sessionStorage.setItem('sellerDetail2', JSON.stringify(res.data));
                this.storeServices.storeDetails = res;
                /*
         res.data.deliveryCharge
          ? this.storeServices.storeServices.push(
              `DELIVERY CHARGES \u20B9${  res.data.deliveryCharge}`
            )
          : this.storeServices.storeServices.push("FREE HOME DELIVERY");
         
        if (res.data.minOrderValue) {
          this.storeServices.storeServices.push(
            `MIN ORDER VALUE \u20B9${  res.data.minOrderValue}`
          );
        }
        if (res.data.shopType == "STORE") {
          this.storeServices.storeServices.push("LOCAL STORE");
        } else if (res.data.shopType) {
          this.storeServices.storeServices.push(`${res.data.shopType  } STORE`);
        }
        if (res.data.acceptedPaymentType == "ALL") {
          this.storeServices.storeServices.push("PAY ON DELIVERY");
          this.storeServices.storeServices.push("ONLINE PAYMENT");
        } else if (res.data.acceptedPaymentType == "POD") {
          this.storeServices.storeServices.push("PAY ON DELIVERY");
        } else if (res.data.acceptedPaymentType == "PG") {
          this.storeServices.storeServices.push("ONLINE PAYMENT");
        }
        this.subjectFOrHome.next(true);
         */
            }
            return Promise.resolve(res.data);
        } catch (err) {
            // console.log(res);
            return Promise.resolve(res.data);
        }
    }

    replaceAll(string, search, replace) {
        return string.split(search).join(replace);
    }

    /* 
 getHome(): Observable<any> {
    return this.subjectFOrHome.asObservable();
  } */
}
