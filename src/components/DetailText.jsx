import React, { useContext } from 'react';
import Cookies from 'js-cookie';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import TextField from '@material-ui/core/TextField';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ProductContext from './ProductContext';
import AppContext from './AppContext';
import AppConfigService from '../services/appconfigservice';

const useStyles = makeStyles(theme => ({
    root: {
        'flex-grow': 1,
        'margin-left': '10px'
    },
    mrp: {
        opacity: '0.3',
        'text-decoration-line': 'line-through'
    },
    media: {
        height: 0,
        paddingTop: '56.25%' // 16:9
    },
    quantity: {
        width: '100px'
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest
        })
    },
    expandOpen: {
        transform: 'rotate(180deg)'
    },
    typography: {
        'white-space': 'pre'
    },
    avatar: {
        backgroundColor: red[500]
    }
}));

export default function DetailText() {
    const classes = useStyles();
    const app = useContext(AppContext);
    const product = useContext(ProductContext);
    console.log(product);
    const availablity = product.quantity ? 'In Stock' : 'Out Of Stock';

    const handleAddToCart = event => {
        event.preventDefault();
        console.log(app);
        app.buyerInfo = Cookies.get('buyerInfo') ? JSON.parse(Cookies.get('buyerInfo')) : '';
        if (app.buyerInfo) {
            const countNO = document.querySelector('#outlined-number').value;
            const obj = {
                count: countNO,
                productId: product.productid,
                userId: app.buyerInfo.buyerid
            };
            AppConfigService.apiService.saveCartItem(obj).then(res => {
                if (res.data.code === '200') {
                }
            });
            return;
        }
        app.showOTP = true;
                            
                        
        app.toggleShowOTP(app);
        
    };
    return (
        <Card className={classes.root} raised="false" elevation="0">
            <CardHeader title={product.productname} subheader={`Availability : ${availablity}`} />

            <CardContent>
                <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                    className={classes.typography}
                >
                    {product.description}
                </Typography>
            </CardContent>
            <CardActions>
                <Typography gutterBottom variant="h6" component="h6">
                    &#8377; {product.sellingprice}
                </Typography>
                {product.sellingprice !== product.mrp && (
                    <Typography gutterBottom className={classes.mrp} variant="h6" component="h6">
                        &#8377; {product.mrp}
                    </Typography>
                )}
                {product.discountPercent && (
                    <Chip
                        variant="outlined"
                        label={`${product.discountPercent} % OFF`}
                        color="secondary"
                    />
                )}
                <TextField
                    id="outlined-number"
                    label="Quantity"
                    type="number"
                    defaultValue="1"
                    InputLabelProps={{
                        shrink: true
                    }}
                    variant="outlined"
                    className={classes.quantity}
                />
                <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={handleAddToCart}
                    className={classes.button}
                    startIcon={<ShoppingCartIcon />}
                >
                    ADD TO CART
                </Button>
            </CardActions>
        </Card>
    );
}
