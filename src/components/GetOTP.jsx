import React, { useContext } from 'react';
import Cookies from 'js-cookie';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import AppContext from './AppContext';
import AppConfigService from '../services/appconfigservice';

export default function FormDialog() {
    const [open, setOpen] = React.useState(true);
    const [disableSubmit, setDisableSubmit] = React.useState(true);
    const [helperText, setHelperText] = React.useState('Enter Mobile Number');
    const [helperTextOtp, setHelperTextOtp] = React.useState('OTP');
    const [showContinue, setShowContinue] = React.useState(false);
    //const [disableContinue, setDisableContinue] = React.useState(true);
    const [disableOtpText, setDisableOtpText] = React.useState(true);
    const app = useContext(AppContext);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleGetOtp = () => {
        const user = {
            username: document.querySelector('#mobileNo').value
        };
        AppConfigService.apiService.getLogin(user).then(res => {
            // console.log(res);
            if (res.data.code === '200' && res.data.data.request_id) {
                setDisableOtpText(false);
                setShowContinue(true);
            } else {
                setHelperText('Please enter correct Number');
                setDisableSubmit(true);
            }
        });
    };
    const handleContinue = () => {
        const user = {
            username: document.querySelector('#mobileNo').value,
            otp: document.querySelector('#otp').value
        };
        AppConfigService.apiService.getLogin(user).then(res => {
            console.log(res);
            if (res.data.code === '200' && res.data.data.buyerid) {
                app.buyerInfo = res.data.data;
                Cookies.set('buyerInfo', app.buyerInfo);
                handleCloseForm();
            } else {
                setHelperTextOtp('Please enter correct OTP');
                setDisableSubmit(true);
            }
        });
    };
    const handleCloseForm = () => {
        setShowContinue(false);
        setDisableOtpText(true);
        app.showOTP = false;
        app.toggleShowOTP(app);
    };
    const hadleOtpChange = event => {
        const val = event.target.value;
        if (Number.isNaN(Number(val))) {
            setHelperTextOtp('Please enter correct mobile number');
            setDisableSubmit(true);
        } else {
            setHelperTextOtp('OTP');
            setDisableSubmit(false);
        }
    };
    const hadleChange = event => {
        const val = event.target.value;
        if (Number.isNaN(Number(val))) {
            setHelperText('Please enter correct OTP');
        } else if (event.target.value.length === 10) {
            setDisableSubmit(false);
            setHelperText('Enter Mobile Number');
        } else {
            setHelperText('Enter Mobile Number');
            setDisableSubmit(true);
        }
    };
    if (app.showOTP) {
        return (
            <div>
                <Dialog
                    open={open}
                    onClose={handleCloseForm}
                    aria-labelledby="form-dialog-title"
                    maxWidth="xs"
                >
                    <DialogContent>
                        <TextField
                            autoFocus
                            required
                            type="tel"
                            InputProps={{
                                inputProps: {
                                    maxLength: 10
                                }
                            }}
                            onChange={hadleChange}
                            margin="dense"
                            id="mobileNo"
                            helperText={helperText}
                            fullWidth
                            size="small"
                        />
                        {!disableOtpText && (
                            <TextField
                                autoFocus
                                required
                                type="text"
                                onChange={hadleOtpChange}
                                margin="dense"
                                id="otp"
                                helperText={helperTextOtp}
                                fullWidth
                                size="small"
                            />
                        )}
                    </DialogContent>
                    <DialogActions>
                        {!showContinue && (
                            <Button onClick={handleGetOtp} color="primary" disabled={disableSubmit}>
                                GET OTP
                            </Button>
                        )}
                        {showContinue && (
                            <Button
                                onClick={handleContinue}
                                color="primary"
                                disabled={disableSubmit}
                            >
                                Continue
                            </Button>
                        )}
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
    return <div></div>;
}
