import React, { useContext, useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Rating from '@material-ui/lab/Rating';
import Button from '@material-ui/core/Button';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import AppContext from './AppContext';

import AppConfigService from '../services/appconfigservice';

const useStyles = makeStyles(theme => ({
    root: {
        'margin-top': 44.75
    },
    header: {
        'background-color': 'antiquewhite'
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest
        })
    },
    cardContentInfo: {
        display: 'flex',
        'align-items': 'flex-end',
        'justify-content': 'space-around'
    },
    cardContent: {
        display: 'flex',
        'flex-wrap': 'wrap',
        'justify-content': 'center',
        'align-content': 'space-between'
    },
    expandOpen: {
        transform: 'rotate(180deg)'
    },
    avatar: {
        backgroundColor: red[500]
    },
    heroButtons: {
        marginTop: theme.spacing(4)
    }
}));

export default function Main() {
    const app = useContext(AppContext);
    const classes = useStyles();
    // const [expanded, setExpanded] = React.useState(false);

    const [storeOffers, setStoreOffers] = useState();
    /*
    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    */
    const params = {
        sellerId: app.sellerid,
        isActive: true
    };
    const apiService = new AppConfigService().getApiService();
    useEffect(() => {
        apiService.getStoreOffer(params).then(data => setStoreOffers(data.data.data));
    }, []);
    return (
        <Container maxWidth="xs">
            <Card className={classes.root}>
                <CardHeader className={classes.header} title={app.shopName} />
                <CardMedia component="img" src={app.images.thumbnailImage} title="Paella dish" />
                <CardContent className={classes.cardContentInfo}>
                    <div>
                        <Typography
                            component="h6"
                            variant="h6"
                            align="center"
                            color="textPrimary"
                            gutterBottom
                        >
                            {app.totalSubscribers}
                        </Typography>
                        <Typography
                            component="h6"
                            variant="h6"
                            align="center"
                            color="textPrimary"
                            gutterBottom
                        >
                            Subscribers
                        </Typography>
                    </div>
                    <div>
                        <Rating
                            name="half-rating-read"
                            defaultValue={app.rating}
                            precision={0.1}
                            size="small"
                            readOnly
                        />
                        <Typography
                            component="h6"
                            variant="h6"
                            align="center"
                            color="textPrimary"
                            gutterBottom
                        >
                            {app.rating}
                        </Typography>
                    </div>
                    <div>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            className={classes.button}
                            startIcon={<BookmarksIcon />}
                        >
                            SUBSCRIBE
                        </Button>
                    </div>
                </CardContent>
                <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {app.shopDescription}
                    </Typography>
                </CardContent>
                <CardContent className={classes.cardContent}>
                    <Chip
                        variant="outlined"
                        label={`MIN ORDER VALUE \u20B9${app.minOrderValue}`}
                        color="primary"
                    />
                    <Chip
                        variant="outlined"
                        label={app.shopType === 'STORE' ? 'LOCAL STORE' : `${app.shopType} STORE`}
                        color="primary"
                    />
                    {app.acceptedPaymentType === 'ALL' ? (
                        ['Pay On Delivery', 'Online Payment'].map(string => (
                            <Chip variant="outlined" label={string} color="primary" />
                        ))
                    ) : app.acceptedPaymentType == 'POD' ? (
                        <Chip variant="outlined" label="Pay On Delivery" color="primary" />
                    ) : (
                        <Chip variant="outlined" label="Online Payment" color="primary" />
                    )}
                    {storeOffers &&
                        storeOffers.map(offer => (
                            <Chip
                                variant="outlined"
                                label={`${offer.discountpercent} % OFF ${offer.categoryName}`}
                                color="secondary"
                            />
                        ))}
                </CardContent>
            </Card>
            {/*
                <CardActions disableSpacing>
                    <IconButton aria-label="add to favorites">
                        <FavoriteIcon />
                    </IconButton>
                    <IconButton aria-label="share">
                        <ShareIcon />
                    </IconButton>
                    <IconButton
                        onClick={handleExpandClick}
                        aria-expanded={expanded}
                        aria-label="show more"
                    >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
                <Collapse in={expanded} timeout="auto" unmountOnExit>
                    <CardContent>
                        <Typography paragraph>Method:</Typography>
                        <Typography paragraph>
                            Heat 1/2 cup of the broth in a pot until simmering, add saffron and set
                            aside for 10 minutes.
                        </Typography>
                        <Typography paragraph>
                            Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over
                            medium-high heat. Add chicken, shrimp and chorizo, and cook, stirring
                            occasionally until lightly browned, 6 to 8 minutes. Transfer shrimp to a
                            large plate and set aside, leaving chicken and chorizo in the pan. Add
                            pimentón, bay leaves, garlic, tomatoes, onion, salt and pepper, and
                            cook, stirring often until thickened and fragrant, about 10 minutes. Add
                            saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
                        </Typography>
                        <Typography paragraph>
                            Add rice and stir very gently to distribute. Top with artichokes and
                            peppers, and cook without stirring, until most of the liquid is
                            absorbed, 15 to 18 minutes. Reduce heat to medium-low, add reserved
                            shrimp and mussels, tucking them down into the rice, and cook again
                            without stirring, until mussels have opened and rice is just tender, 5
                            to 7 minutes more. (Discard any mussels that don’t open.)
                        </Typography>
                        <Typography>
                            Set aside off of the heat to let rest for 10 minutes, and then serve.
                        </Typography>
                    </CardContent>
                </Collapse>
            </Card>
                */}
            {/* <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Album layout
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
                Something short and leading about the collection below—its contents, the creator,
                etc. Make it short and sweet, but not too short so folks don&apos;t simply skip over
                it entirely.
            </Typography>
            <div className={classes.heroButtons}>
                <Grid container spacing={2} justify="center">
                    <Grid item>
                        <Button variant="contained" color="primary">
                            Main call to action
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button variant="outlined" color="primary">
                            Secondary action
                        </Button>
                    </Grid>
                </Grid>
            </div> */}
        </Container>
    );
}
