import React, { useContext } from "react";
import { Link as RouteLink} from 'react-router-dom';
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import CssBaseline from "@material-ui/core/CssBaseline";
import DescriptionIcon from "@material-ui/icons/Description";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";
import PhoneIcon from "@material-ui/icons/Phone";
import EmailIcon from "@material-ui/icons/Email";
import AppContext from "./AppContext";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
  },
  main: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(2)
  },
  footer: {
    padding: theme.spacing(3, 2),
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[800]
  },
  iconsWrapper: {
    "flex-flow": "nowrap"
  },
  website: {
    "text-align": "center"
  }
}));

export default function AppFooter() {
  const app = useContext(AppContext);
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <footer className={classes.footer}>
        <Container maxWidth="xl" disableGutters={false}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={3}>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <Typography variant="h6" align="center" gutterBottom>
                    Description
                  </Typography>
                </Grid>
                <Grid item>
                  <Grid container spacing={2} className={classes.iconsWrapper}>
                    <Grid item>
                      <DescriptionIcon fontSize="large" />
                    </Grid>
                    <Grid item>
                      <Typography variant="body1" gutterBottom>
                        {app.shopDescription}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <Typography variant="h6" align="center" gutterBottom>
                    Location
                  </Typography>
                </Grid>
                <Grid item>
                  <Grid container spacing={2} className={classes.iconsWrapper}>
                    <Grid item>
                      <LocationOnIcon fontSize="large" />
                    </Grid>
                    <Grid item>
                      <Typography variant="body1" gutterBottom>
                        {app.address}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <Typography variant="h6" align="center" gutterBottom>
                    Contact Us
                  </Typography>
                </Grid>
                <Grid item>
                  <Grid container spacing={2} direction="column">
                    <Grid item>
                      <Grid
                        container
                        spacing={2}
                        className={classes.iconsWrapper}
                      >
                        <Grid item>
                          <PhoneIcon fontSize="large" />
                        </Grid>
                        <Grid item>
                          <Typography variant="body1" gutterBottom>
                            {app.mobileNo}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Grid
                        container
                        spacing={2}
                        className={classes.iconsWrapper}
                      >
                        <Grid item>
                          <EmailIcon fontSize="large" />
                        </Grid>
                        <Grid item>
                          <Typography variant="body1" gutterBottom>
                            {app.emailId}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <Typography variant="h6" align="center" gutterBottom>
                    Terms & Condition
                  </Typography>
                </Grid>
                <Grid item>
                  <Grid container spacing={2} className={classes.iconsWrapper}>
                    <Grid item>
                      <ConfirmationNumberIcon fontSize="large" />
                    </Grid>
                    <Grid item>

                    <RouteLink to="/tnc">TNC</RouteLink>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item justify="center">
            <Typography className={classes.website}>
              <Link
                underline="hover"
                href="https://play.google.com/store/apps/details?id=in.bybuy.www.seller"
              >
                Powered by ByBuy Seller App.
              </Link>
            </Typography>
          </Grid>
        </Container>
      </footer>
    </div>
  );
}
