import React, { useContext, useState, useEffect } from 'react';
import { Link as RouteLink, useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Paper } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { autoPlay } from 'react-swipeable-views-utils';
import AppContext from './AppContext';
import AppConfigService from '../services/appconfigservice';
import ProductContext from './ProductContext';
import DetailText from './DetailText';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
const useStyles = makeStyles(theme => ({
    parent: {
        display: 'flex',
        'flex-wrap': 'wrap',
        margin: '44px !important'
    },
    root: {
        maxWidth: 400,
        flexGrow: 1
    },
    header: {
        display: 'flex',
        alignItems: 'center',
        height: 50,
        paddingLeft: theme.spacing(4),
        backgroundColor: theme.palette.background.default
    },
    paperCard: {
        display: 'flex',
        'flex-grow': 1,
        'flex-wrap': 'wrap',
        margin: '44px',
        padding: '10px'
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    cardMedia: {
        paddingTop: '56.25%' // 16:9
    },
    cardContent: {
        flexGrow: 1
    },
    img: {
        height: 255,
        display: 'block',
        maxWidth: 400,
        overflow: 'hidden',
        width: '100%'
    },
    accordin: {
        width: '100%'
    },
    routeLink: {
        'text-decoration': 'none'
    },
    accordinContent: {
        'justify-content': 'center'
    },
    accordinSummary: {
        'justify-content': 'center'
    },
    accordinDetailRoot: {
        'justify-content': 'center',
        display: 'flex',
        'flex-wrap': 'wrap'
    }
}));
export default function Detail(props) {
    const app = useContext(AppContext);
    const { id } = useParams();
    const [propValue, setPropValue] = useState(props);
    let [products, setProducts] = useState([]);
    const [productData, setProductData] = useState({ images: { images: [] }, thumbnailImage: '' });
    console.log(id);
    const classes = useStyles();
    const apiService = new AppConfigService().getApiService();
    const params = {
        sellerid: app.sellerid,
        latitude: app.latitude,
        longitude: app.longitude,
        productId: id,
        isSellerActive: true,
        page: 0,
        size: 10,
        isProductActive: true
    };
    const index = 0;

    useEffect(() => {
        apiService
            .getProDetail({ prodId: id })
            .then(data => {
                window.scrollTo(0, 0);
                data.data.data.images.images.push(data.data.data.images.thumbnailImage);
                return setProductData(data.data.data);
            })
            .then(() =>
                apiService.getSimilarProd(params).then(data => setProducts(data.data.data))
            );
    }, [props]);
    //data.data.data.images.images.push(data.data.data.images.thumbnailImage)
    //productData.images.images.push(productData.images.thumbnailImage)
    if (products.content) {
        products = products.content;
    }
    console.log(products);

    const [activeStep, setActiveStep] = React.useState(0);
    const maxSteps = productData.images.images.length;

    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleStepChange = step => {
        setActiveStep(step);
    };

    return (
        <div>
            <div className={classes.parent}>
                <ProductContext.Provider value={productData}>
                    <div className={classes.root}>
                        <AutoPlaySwipeableViews
                            axis="x"
                            index={activeStep}
                            onChangeIndex={handleStepChange}
                            enableMouseEvents
                        >
                            {productData.images.images.map((step, index) => (
                                <div key={step}>
                                    {Math.abs(activeStep - index) <= 2 ? (
                                        <img className={classes.img} src={step} alt={step} />
                                    ) : null}
                                </div>
                            ))}
                        </AutoPlaySwipeableViews>
                        <MobileStepper
                            steps={maxSteps}
                            position="static"
                            variant="text"
                            activeStep={activeStep}
                            nextButton={
                                <Button
                                    size="small"
                                    onClick={handleNext}
                                    disabled={activeStep === maxSteps - 1}
                                >
                                    Next
                                    <KeyboardArrowRight />
                                </Button>
                            }
                            backButton={
                                <Button
                                    size="small"
                                    onClick={handleBack}
                                    disabled={activeStep === 0}
                                >
                                    <KeyboardArrowLeft />
                                    Back
                                </Button>
                            }
                        />
                    </div>
                    <DetailText />
                </ProductContext.Provider>
            </div>
            <div className={classes.parent}>
                <Accordion className={classes.accordin} expanded={true}>
                    <AccordionSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        className={classes.accordinSummary}
                        classes={{ content: classes.accordinSummary }}
                    >
                        <Typography className={classes.accordinSummary}>SIMILAR ITEMS</Typography>
                    </AccordionSummary>
                    <AccordionDetails classes={{ root: classes.accordinDetailRoot }}>
                        <Grid container spacing={4} key={`container-${index}`}>
                            {products.length > 0 ? (
                                products.map(product => (
                                    <Grid
                                        item
                                        key={product.productid}
                                        xs={12}
                                        sm={6}
                                        md={4}
                                        lg={3}
                                        key={`tab-${index}-grid-item-${product.productid}`}
                                    >
                                        <RouteLink
                                            to={`/detail/${product.productid}`}
                                            key={`tab-${index}-${product.productid}`}
                                            className={classes.routeLink}
                                        >
                                            <Card
                                                className={classes.card}
                                                id={`tab-${index}-card-${product.productid}`}
                                            >
                                                <CardMedia
                                                    className={classes.cardMedia}
                                                    image={product.images.thumbnailImage}
                                                    title={product.productname}
                                                    id={`tab-${index}-card-media-${product.productid}`}
                                                />
                                                <CardContent
                                                    className={classes.cardContent}
                                                    key={`tab-${index}-card-content-${product.productid}`}
                                                >
                                                    <Typography
                                                        gutterBottom
                                                        variant="h5"
                                                        component="h2"
                                                    >
                                                        {product.productname}
                                                    </Typography>
                                                </CardContent>

                                                <CardActions
                                                    key={`tab-${index}-card-actions-${product.productid}`}
                                                >
                                                    <Typography
                                                        gutterBottom
                                                        variant="h6"
                                                        component="h6"
                                                    >
                                                        &#8377; {product.sellingprice}
                                                    </Typography>
                                                    {product.discountPercent && (
                                                        <Typography
                                                            gutterBottom
                                                            className={classes.mrp}
                                                            variant="h6"
                                                            component="h6"
                                                        >
                                                            &#8377; {product.mrp}
                                                        </Typography>
                                                    )}
                                                    {product.discountPercent && (
                                                        <Chip
                                                            variant="outlined"
                                                            label={`${product.discountPercent} % OFF`}
                                                            color="secondary"
                                                        />
                                                    )}
                                                </CardActions>
                                            </Card>
                                        </RouteLink>
                                    </Grid>
                                ))
                            ) : (
                                <Grid
                                    className={classes.emptyCard}
                                    item
                                    key={0}
                                    xs={12}
                                    sm={12}
                                    md={12}
                                    lg={12}
                                >
                                    <div>
                                        {' '}
                                        <Typography gutterBottom variant="h6" component="h6">
                                            Product is not available.
                                        </Typography>
                                    </div>
                                </Grid>
                            )}
                        </Grid>
                    </AccordionDetails>
                </Accordion>
            </div>
        </div>
    );
}
