import React, { useContext, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Paper } from '@material-ui/core';
import AppContext from './AppContext';

const useStyles = makeStyles(() => ({
    paper: {
        margin: '20px'
    },
    heading: {
        'text-align': 'center'
    },
    text: {
        margin: '20px',
        'text-align': 'justify'
    }
}));
export default function TNC() {
    const app = useContext(AppContext);
    const classes = useStyles();
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    return (
        <Paper elevation={3} className={classes.paper}>
            <Typography variant="h1" className={classes.heading}>
                Terms and Conditions
            </Typography>
            <Typography variant="body1" className={classes.text}>
                {app.tnc}
            </Typography>
        </Paper>
    );
}
