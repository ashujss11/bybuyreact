import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import AppContext from './AppContext';
import AppFooter from './AppFooter';
import Main from './Main';
import Topbar from './Topbar';
import TNC from './TNC';
import Detail from './Detail';
import FormDialog from './GetOTP';
import Checkout from './Checkout';

import AppConfigService from '../services/appconfigservice';

const useStyles = makeStyles({
    root: {
        display: 'flex',
        'flex-direction': 'column',
        flexGrow: 1
    },
    loader: {
        display: 'flex',
        'flex-direction': 'row',
        'justify-content': 'center',
        'align-items': 'center',
        height: '100%'
    }
});

export default function App() {
    const classes = useStyles();
    const [sellerDetails, setSellerDetails] = useState();
    const appConfigService = new AppConfigService();
    const toggleShowOTP = obj => {
        console.log(sellerDetails);
        setSellerDetails({ ...obj });
    };
    useEffect(() => {
        appConfigService
            .getSomeData()
            .then(data => setSellerDetails({ showOTP: false, toggleShowOTP, ...data }));
    }, []);

    if (sellerDetails) {
        return (
            <>
                <AppContext.Provider value={sellerDetails}>
                    <CssBaseline />
                    <Container maxWidth="xl" disableGutters>
                        <div className={classes.root}>
                            <Router>
                                <Topbar />
                                <FormDialog />
                                <Switch>
                                    <Route exact path="/" component={Main} />
                                    <Route path="/tnc" component={TNC} />
                                    <Route
                                        exact
                                        path="/detail/:id"
                                        render={props => {
                                            props = [Math.random()];
                                            return <Detail {...props} value={Math.random()} />;
                                        }}
                                    ></Route>
                                    <Route path="/checkout" component={Checkout} />
                                </Switch>
                                <AppFooter />
                            </Router>
                        </div>
                    </Container>
                </AppContext.Provider>
            </>
        );
    }
    return (
        <div className={classes.loader}>
            <CircularProgress />
        </div>
    );
}
