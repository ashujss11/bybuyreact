import React, { useContext, useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import AppContext from './AppContext';
import AppConfigService from '../services/appconfigservice';
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        display: 'flex',
        'flex-direction': 'row',
        'flex-wrap': 'wrap'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)'
    },
    title: {
        fontSize: 14
    },
    information: {
        display: 'flex',
        'justify-content': 'space-between'
    },
    buttons: {
        display: 'flex',
        'justify-content': 'space-between'
    },
    pos: {
        marginBottom: 12
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7)
    }
}));
export default function Cart() {
    const classes = useStyles();
    const app = useContext(AppContext);
    app.buyerInfo = Cookies.get('buyerInfo') ? JSON.parse(Cookies.get('buyerInfo')) : '';
    const [reviewDetails, setReviewDetails] = useState([]);
    const [products, setProducts] = useState([]);
    useEffect(() => {
        AppConfigService.apiService
            .getFinalItem({
                buyerId: app.buyerInfo.buyerid,
                sellerId: app.sellerid,
                deliveryAddressId: 7
            })
            .then(res => {
                if (res.data.code === '200') {
                    setReviewDetails(res.data.data);
                    const productList = [];
                    res.data.data.sellerDetails[0].categories.forEach(category => {
                        category.items.forEach(item => productList.push(item));
                    });
                    setProducts(productList);
                }
            });
    }, []);

    const updateCount = (item, flag) => {
        const cnt = item.count;
        if (item.count >= 5 && flag == 'inc') {
            return;
        }
        if (flag == 'delete' || (item.count == 1 && flag == 'dsc')) {
            const itemRmvParam = {
                productId: item.productid,
                userId: app.buyerInfo.buyerid
            };
            AppConfigService.apiService.removeCartItem(name, itemRmvParam).then(data => {
                const productsClone = [];
                products.map(p => {
                    if (p.productid === item.productid) {
                        p.count = itemParam.count;
                    }
                    productsClone.push(p);
                });
                setProducts(productsClone);
                console.log(data, products);
            });
        } else {
            const itemParam = {
                productId: item.productid,
                userId: app.buyerInfo.buyerid,
                count: flag == 'inc' ? cnt + 1 : cnt - 1
            };
            AppConfigService.apiService.updateCartItem(name, itemParam).then(data => {
                const productsClone = [];
                products.map(p => {
                    if (p.productid === item.productid) {
                        p.count = itemParam.count;
                    }
                    productsClone.push(p);
                });
                setProducts(productsClone);
                console.log(data, products);
            });
        }
    };

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Cart Details
            </Typography>
            {products.map(product => (
                <Card className={classes.root} variant="outlined">
                    <CardContent className={classes.root}>
                        <Avatar
                            variant="square"
                            alt={`Avatar n°1`}
                            className={classes.large}
                            src={product.productThumbnailImage}
                        ></Avatar>
                        <div>
                            <h3>{product.productName}</h3>
                            <div className={classes.information}>
                                <p>Price: {product.productMrp}</p>
                                <p>
                                    Total: {(product.count * product.totalSellingPrice).toFixed(2)}
                                </p>
                            </div>
                            <div className={classes.buttons}>
                                <Button
                                    size="small"
                                    disableElevation
                                    variant="contained"
                                    onClick={() => updateCount(product, 'dsc')}
                                >
                                    -
                                </Button>
                                <p>{product.count}</p>
                                <Button
                                    size="small"
                                    disableElevation
                                    variant="contained"
                                    onClick={() => updateCount(product, 'inc')}
                                >
                                    +
                                </Button>
                            </div>
                        </div>
                    </CardContent>
                </Card>
            ))}
        </React.Fragment>
    );
}
