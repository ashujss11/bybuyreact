import React, { useContext } from 'react';
import $ from 'jquery';
import Cookies from 'js-cookie';
import * as CryptoJS from 'crypto-js';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import AddressForm from './AddressForm';
import PaymentForm from './PaymentForm';
import Review from './Review';
import Cart from './Cart';
import AppConfigService from '../services/appconfigservice';
import AppContext from './AppContext';

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative'
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto'
        }
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3)
        }
    },
    stepper: {
        padding: theme.spacing(3, 0, 5)
    },
    stepperContent: { display: 'flex', 'flex-direction': 'column', 'align-items': 'center' },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1)
    }
}));

const steps = ['Cart', 'Shipping address', 'Payment details', 'Review your order'];

export default function Checkout(props) {
    let initialStep = 0;
    let initialOrderDetail = '';

    if (Cookies.get('response')) {
        const orderResponse = JSON.parse(Cookies.get('response').replace('j:', ''));
        const transactionStatus = orderResponse ? orderResponse.txStatus : '';

        if (transactionStatus === 'SUCCESS') {
            initialStep = 4;
            initialOrderDetail = `Your Order is Placed Successfully with order id ${orderResponse.orderId}, To Track and manage your Orders, Download ByBuy App`;

            const orderparam = {
                orderId: orderResponse.orderId
            };
            AppConfigService.apiService.getCashOrderStatus(orderparam).then(res => {
                let firstArgument = {};
                let secondArgument = '';
                if (res.status == '200') {
                    firstArgument = res.data.data;
                    secondArgument =
                        res.data.data.transactionDetails.indexOf('SUCCESS') >= 0
                            ? 'PG_SUCCESS'
                            : 'PG_FAILURE';
                } else {
                    firstArgument = { transactionDetails: '' };
                    secondArgument = 'PG_FAILURE';
                }
                const offerparam = {
                    orderId: orderResponse.orderId,
                    paymentMode: secondArgument,
                    cashfree: firstArgument.transactionDetails
                };
                AppConfigService.apiService.getOrderStatus(offerparam).then(res => {
                    if (res.status == '200') {
                        if (secondArgument === 'PG_SUCCESS') {
                            initialOrderDetail = `Your Order is Placed Successfully with order id ${orderResponse.orderId}, To Track and manage your Orders, Download ByBuy App`;
                        } else {
                            initialOrderDetail = `Your Order has failed. Please try again.`;
                        }
                    }
                });
            });
            Cookies.remove('response');
        }
    }
    const [activeStep, setActiveStep] = React.useState(initialStep);
    const app = useContext(AppContext);
    const classes = useStyles();

    const [orderDetail, setOrderDetail] = React.useState(initialOrderDetail);
    const handleNext = () => {
        if (event.target.innerText === 'PLACE ORDER') {
            orderCall();
        }
        setActiveStep(activeStep + 1);
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };
    const cashFree = () => {
        if (
            !JSON.parse(sessionStorage.getItem('orderDetail')).pgDetails &&
            !JSON.parse(sessionStorage.getItem('orderDetail')).pgDetails.pgKey
        ) {
            return;
        }
        var postData = {
                appId: JSON.parse(sessionStorage.getItem('orderDetail')).pgDetails.pgKey,
                orderId: JSON.parse(sessionStorage.getItem('orderDetail')).orderId,
                orderAmount: JSON.parse(sessionStorage.getItem('orderDetail')).totalSellingPrice,
                orderCurrency: 'INR',
                orderNote: 'test',
                customerName: JSON.parse(sessionStorage.getItem('orderDetail')).deliveryAddress
                    .name,
                customerEmail: app.buyerInfo.emailId,
                customerPhone: app.buyerInfo.mobileNo,
                returnUrl: window.location.origin + '/checkout',
                notifyUrl: window.location.origin + '/checkout',
                signature: ''
            },
            secretKey = JSON.parse(sessionStorage.getItem('orderDetail')).pgDetails.pgSecret,
            sortedkeys = Object.keys(postData),
            signatureData = '';
        sortedkeys.sort();
        for (var i = 0; i < sortedkeys.length; i++) {
            var k = sortedkeys[i];
            if (k != 'signature') signatureData += k + postData[k];
        }
        const signature2 = CryptoJS.HmacSHA256(signatureData, secretKey);
        const signatureBase = CryptoJS.enc.Base64.stringify(signature2);
        postData.signature = signatureBase;
        //this.postData = postData;
        var url = 'https://www.cashfree.com/checkout/post/submit';
        var form = $(
            '<form action="' +
                url +
                '" method="post">' +
                '<input type="hidden" name="appId" value="' +
                postData.appId +
                '"/>' +
                '<input type="hidden" name="orderId" value="' +
                postData.orderId +
                '"/>' +
                '<input type="hidden" name="orderAmount" value="' +
                postData.orderAmount +
                '"/>' +
                '<input type="hidden" name="orderCurrency" value="' +
                postData.orderCurrency +
                '"/>' +
                '<input type="hidden" name="orderNote" value="' +
                postData.orderNote +
                '" />' +
                '<input type="hidden" name="customerName" value="' +
                postData.customerName +
                '" />' +
                '<input type="hidden" name="customerEmail" value="' +
                postData.customerEmail +
                '" />' +
                '<input type="hidden" name="customerPhone" value="' +
                postData.customerPhone +
                '" />' +
                '<input type="hidden" name="returnUrl" value="' +
                postData.returnUrl +
                '" />' +
                '<input type="hidden" name="notifyUrl" value="' +
                postData.notifyUrl +
                '" />' +
                '<input type="hidden" name="signature" value="' +
                postData.signature +
                '" />' +
                '</form>'
        );
        $('body').append(form);
        form.submit();
    };
    const orderCall = () => {
        var pmt = '';
        if (app.paymentType === 'OP') {
            pmt = 'PG';
        } else {
            pmt = 'POD';
        }

        var offerparam = {
            buyerId: app.buyerInfo.buyerid,
            sellerId: app.sellerid,
            deliveryAddressId: JSON.parse(sessionStorage.getItem('selectedAddress')).addressid,
            paymentMode: pmt
        };
        AppConfigService.apiService.getGenerateOrder(offerparam).then(res => {
            if (res.status == '200') {
                sessionStorage.setItem('orderDetail', JSON.stringify(res.data.data));

                if (pmt == 'POD') {
                    AppConfigService.apiService
                        .getOrderStatus({ orderId: res.data.data.orderId, paymentMode: pmt })
                        .then(resposne => {
                            if (resposne.status == '200') {
                                setOrderDetail(
                                    `Your Order is Placed Successfully with order id ${res.data.data.orderId}, To Track and manage your Orders, Download ByBuy App`
                                );
                            } else {
                            }
                        });
                } else {
                    cashFree();
                }
            } else {
            }
        });
    };
    const getStepContent = step => {
        switch (step) {
            case 0:
                return <Cart />;
            case 1:
                return <AddressForm handleNext={handleNext} />;
            case 2:
                return <PaymentForm />;
            case 3:
                return <Review />;
            default:
                throw new Error('Unknown step');
        }
    };

    return (
        <React.Fragment>
            <CssBaseline />

            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h4" align="center">
                        Checkout
                    </Typography>
                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {steps.map(label => (
                            <Step key={label}>
                                <StepLabel className={classes.stepperContent}>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <React.Fragment>
                        {activeStep === steps.length ? (
                            <React.Fragment>
                                <Typography variant="h5" gutterBottom>
                                    Thank you for your order.
                                </Typography>
                                <Typography variant="subtitle1">{orderDetail}</Typography>
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                {getStepContent(activeStep)}
                                <div className={classes.buttons}>
                                    {activeStep !== 0 && (
                                        <Button onClick={handleBack} className={classes.button}>
                                            Back
                                        </Button>
                                    )}
                                    {activeStep != 1 && (
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={handleNext}
                                            className={classes.button}
                                        >
                                            {activeStep === steps.length - 1
                                                ? 'Place order'
                                                : 'Next'}
                                        </Button>
                                    )}
                                </div>
                            </React.Fragment>
                        )}
                    </React.Fragment>
                </Paper>
            </main>
        </React.Fragment>
    );
}
