import React, { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Cookies from 'js-cookie';
import AppBar from '@material-ui/core/AppBar';
import { fade, makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import AppContext from './AppContext';

const useStyles = makeStyles(theme => ({
    link: {
        color: 'white'
    },
    toolbar: {
        display: 'flex',
        'justify-content': 'space-between'
    },
    homeButton: {
        marginRight: theme.spacing(2)
    },
    checkoutButton: {
        marginLeft: theme.spacing(2)
    },
    /*
  title: {
    flexGrow: 1,
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  */
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginLeft: 0,
        width: '50% !important',
        [theme.breakpoints.up('lg')]: {
            marginLeft: theme.spacing(1),
            width: 'auto'
        }
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputRoot: {
        color: 'inherit'
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch'
            }
        }
    }
}));

export default function Topbar() {
    const app = useContext(AppContext);
    const history = useHistory();
    const topbarClasses = useStyles();
    return (
        <AppBar position="static">
            <Toolbar className={topbarClasses.toolbar}>
                <IconButton
                    edge="start"
                    className={topbarClasses.homeButton}
                    color="inherit"
                    aria-label="Home"
                >
                    <Link className={topbarClasses.link} to="/">
                        <HomeIcon />
                    </Link>
                </IconButton>
                <div className={topbarClasses.search}>
                    <div className={topbarClasses.searchIcon}>
                        <SearchIcon />
                    </div>
                    <InputBase
                        placeholder="Search…"
                        classes={{
                            root: topbarClasses.inputRoot,
                            input: topbarClasses.inputInput
                        }}
                        inputProps={{
                            'aria-label': 'search'
                        }}
                    />
                </div>
                <IconButton
                    edge="start"
                    className={topbarClasses.checkoutButton}
                    color="inherit"
                    aria-label="Shopping Cart"
                    onClick={() => {
                        app.buyerInfo = Cookies.get('buyerInfo')
                            ? JSON.parse(Cookies.get('buyerInfo'))
                            : '';
                        if (app.buyerInfo) {
                            history.push('/checkout');
                            return;
                        }
                        app.showOTP = true;
                        app.toggleShowOTP(app);
                    }}
                >
                    <ShoppingCartIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
    );
}
