import React, { useContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import { Link as RouteLink } from 'react-router-dom';
import Badge from '@material-ui/core/Badge';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Chip from '@material-ui/core/Chip';
import Tab from '@material-ui/core/Tab';
import StoreDetails from './StoreDetails';
import AppContext from './AppContext';
import AppConfigService from '../services/appconfigservice';

const useStyles = makeStyles(theme => ({
    main: {
        display: 'flex',
        'flex-wrap': 'wrap'
    },
    mrp: {
        opacity: '0.3',
        'text-decoration-line': 'line-through'
    },
    tabPanel: {
        padding: '20px'
    },
    icon: {
        marginRight: theme.spacing(2)
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6)
    },
    heroButtons: {
        marginTop: theme.spacing(4)
    },
    routeLink: {
        'text-decoration':'none'
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8)
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    cardMedia: {
        paddingTop: '56.25%' // 16:9
    },
    cardContent: {
        flexGrow: 1
    },
    emptyCard: {
        'margin-top': '20%',
        display: 'inline-flex',
        'align-content': 'center',
        'justify-content': 'center',
        height: 'auto'
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6)
    }
}));

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function a11yProps(index) {
    return {
        id: `scrollable-force-tab-${index}`,
        'aria-controls': `scrollable-force-tabpanel-${index}`
    };
}

function TabPanel(props) {
    const params = {
        sellerid: JSON.parse(sessionStorage.getItem('sellerDetail2')).sellerid,
        categoryid: '',
        isSellerActive: true,
        page: 0,
        size: 10,
        isProductActive: true
    };

    const { children, value, index, ...other } = props;
    const classes = useStyles();
    params.categoryid = index === -1 ? '' : index;
    const [products, setProducts] = useState([]);
    const apiService = new AppConfigService().getApiService();
    useEffect(() => {
        apiService.getproductData(params).then(data => setProducts([...data.data.data.content]));
    }, []);

    console.log(products);

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            key={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Grid container spacing={4} key={`container-${index}`}>
                    {products.length > 0 ? (
                        products.map(product => (
                            <Grid
                                item
                                key={product.productid}
                                xs={12}
                                sm={6}
                                md={4}
                                lg={3}
                                key={`tab-${index}-grid-item-${product.productid}`}
                            >
                                <RouteLink
                                    to={`/detail/${product.productid}`}
                                    key={`tab-${index}-${product.productid}`}
                                    className={classes.routeLink}
                                >
                                    <Card
                                        className={classes.card}
                                        id={`tab-${index}-card-${product.productid}`}
                                    >
                                        <CardMedia
                                            className={classes.cardMedia}
                                            image={product.images.thumbnailImage}
                                            title={product.productname}
                                            id={`tab-${index}-card-media-${product.productid}`}
                                        />
                                        <CardContent
                                            className={classes.cardContent}
                                            key={`tab-${index}-card-content-${product.productid}`}
                                        >
                                            <Typography gutterBottom variant="h5" component="h2">
                                                {product.productname}
                                            </Typography>
                                        </CardContent>

                                        <CardActions
                                            key={`tab-${index}-card-actions-${product.productid}`}
                                        >
                                            <Typography gutterBottom variant="h6" component="h6">
                                                &#8377; {product.sellingprice}
                                            </Typography>
                                            {product.discountPercent && (
                                                <Typography
                                                    gutterBottom
                                                    className={classes.mrp}
                                                    variant="h6"
                                                    component="h6"
                                                >
                                                    &#8377; {product.mrp}
                                                </Typography>
                                            )}
                                            {product.discountPercent && (
                                                <Chip
                                                    variant="outlined"
                                                    label={`${product.discountPercent} % OFF`}
                                                    color="secondary"
                                                />
                                            )}
                                        </CardActions>
                                    </Card>
                                </RouteLink>
                            </Grid>
                        ))
                    ) : (
                        <Grid
                            className={classes.emptyCard}
                            item
                            key={0}
                            xs={12}
                            sm={12}
                            md={12}
                            lg={12}
                        >
                            <div>
                                {' '}
                                <Typography gutterBottom variant="h6" component="h6">
                                    Product is not available.
                                </Typography>
                            </div>
                        </Grid>
                    )}
                </Grid>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired
};

export default function Main() {
    const classes = useStyles();
    const app = useContext(AppContext);
    const [value, setValue] = React.useState(-1);
    const [categories, setCategories] = useState([]);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const apiService = new AppConfigService().getApiService();
    const params = {
        sellerId: app.sellerid
    };
    useEffect(() => {
        apiService.getCategories(params).then(data =>
            setCategories([
                {
                    categoryid: -1,
                    name: 'All',
                    categoryImage: '',
                    carousalImage: '',
                    bgColorHexCode: '#77d8d8',
                    appDisplayPriorityNumber: null,
                    createdOn: 1599653399346,
                    modifiedOn: 1599653399346
                },
                ...data.data.data
            ])
        );
    }, []);
    return (
        <main className={classes.main}>
            {/* Hero unit */}
            <StoreDetails key="storeDetails" />
            <Container className={classes.cardGrid} maxWidth="lg" key="mainContainer">
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        key="tabs"
                        aria-label="scrollable auto tabs example"
                    >
                        {categories.map(category => (
                            <Tab
                                key={category.categoryid}
                                value={category.categoryid}
                                label={category.name}
                                {...a11yProps(0)}
                            />
                        ))}
                    </Tabs>
                </AppBar>
                {categories.map(category => (
                    <TabPanel
                        className={classes.tabPanel}
                        value={value}
                        key={`tabpanel-${category.categoryid}`}
                        index={category.categoryid}
                    />
                ))}
            </Container>
        </main>
    );
}
