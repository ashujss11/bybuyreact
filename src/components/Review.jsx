import React, { useContext, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import AppConfigService from '../services/appconfigservice';
import AppContext from './AppContext';

const payments = [
    { name: 'Card type', detail: 'Visa' },
    { name: 'Card holder', detail: 'Mr John Smith' },
    { name: 'Card number', detail: 'xxxx-xxxx-xxxx-1234' },
    { name: 'Expiry date', detail: '04/2024' }
];

const useStyles = makeStyles(theme => ({
    listItem: {
        padding: theme.spacing(1, 0)
    },
    total: {
        fontWeight: 700
    },
    title: {
        marginTop: theme.spacing(2)
    },
    mrp: {
        opacity: '0.3',
        'text-decoration-line': 'line-through'
    }
}));

export default function Review() {
    const classes = useStyles();
    const app = useContext(AppContext);
    const [reviewDetails, setReviewDetails] = useState([]);
    const [products, setProducts] = useState([]);
    useEffect(() => {
        AppConfigService.apiService
            .getFinalItem({
                buyerId: app.buyerInfo.buyerid,
                sellerId: app.sellerid,
                deliveryAddressId: app.selectedAddress
            })
            .then(res => {
                if (res.data.code === '200') {
                    setReviewDetails(res.data.data);
                    const productList = [];
                    res.data.data.sellerDetails[0].categories.forEach(category => {
                        category.items.forEach(item => productList.push(item));
                    });
                    setProducts(productList);
                }
            });
    }, []);
    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Order summary
            </Typography>
            <List disablePadding>
                {products.map(product => (
                    <ListItem className={classes.listItem} key={product.name}>
                        <ListItemAvatar>
                            <Avatar alt={`Avatar n°1`} src={product.productThumbnailImage}></Avatar>
                        </ListItemAvatar>
                        <ListItemText
                            primary={product.productName}
                            secondary={`Quantity : ${product.count}`}
                        />
                        <Typography className={classes.mrp} variant="body2">
                            {`${product.productMrp} `}
                        </Typography>
                        <Typography variant="body2">{product.totalSellingPrice}</Typography>
                    </ListItem>
                ))}
                <ListItem className={classes.listItem}>
                    <ListItemText primary="Total" />
                    <Typography variant="subtitle1" className={classes.total}>
                        {reviewDetails.totalSellingPrice}
                    </Typography>
                </ListItem>
            </List>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <Typography variant="h6" gutterBottom className={classes.title}>
                        Shipping
                    </Typography>
                    <Typography gutterBottom>
                        {reviewDetails.deliveryAddress && reviewDetails.deliveryAddress.name}
                    </Typography>
                    <Typography gutterBottom>
                        {reviewDetails.deliveryAddress && reviewDetails.deliveryAddress.address}
                    </Typography>
                    <Typography gutterBottom>
                        {reviewDetails.deliveryAddress && reviewDetails.deliveryAddress.mobileNo}
                    </Typography>
                </Grid>
                <Grid item container direction="column" xs={12} sm={6}>
                    <Typography variant="h6" gutterBottom className={classes.title}>
                        Payment details
                    </Typography>
                    {app.paymentType === 'POD' && (
                        <Typography gutterBottom>Payment on Delivery</Typography>
                    )}
                    {app.paymentType === 'OP' && (
                        <Typography gutterBottom>Online Payment</Typography>
                    )}
                </Grid>
            </Grid>
        </React.Fragment>
    );
}
