import React, { useContext, useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import AppContext from './AppContext';

export default function PaymentForm() {
    const app = useContext(AppContext);

    const [value, setValue] = React.useState(app.paymentType || 'POD');
    app.paymentType = value;
    const handleRadioChange = event => {
        app.paymentType = event.target.value;
        setValue(event.target.value);
    };

    return (
        <React.Fragment>
            <Typography variant="h6" gutterBottom>
                Payment method
            </Typography>
            <RadioGroup aria-label="quiz" name="quiz" value={value} onChange={handleRadioChange}>
                <FormControlLabel
                    key="POD"
                    value="POD"
                    control={<Radio />}
                    label="Payement On Delivery"
                />
                <FormControlLabel key="OP" value="OP" control={<Radio />} label="Online Payement" />
            </RadioGroup>
        </React.Fragment>
    );
}
