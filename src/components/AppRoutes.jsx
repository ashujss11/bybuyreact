import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import App from './App';

export default function AppRoutes() {
    return (
        <div>
            <Router>
                <App/>
                <Switch>
                    <Route path="/" component={App} />
                </Switch>
            </Router>
        </div>
    );
}
