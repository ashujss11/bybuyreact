import React, { useContext, useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Cookies from 'js-cookie';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import AppConfigService from '../services/appconfigservice';
import AppContext from './AppContext';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(3)
    },
    button: {
        margin: theme.spacing(1, 1, 0, 0)
    },
    dividerInset: {
        margin: `5px 0 0 ${theme.spacing(9)}px`
    }
}));

const getCheckedAddress = addresses =>
    addresses.filter(address => address.checked)[0].addressid.toString();

export default function AddressForm(props) {
    const classes = useStyles();
    const app = useContext(AppContext);
    const [buyerAddress, setbuyerAddress] = useState([]);
    const [value, setValue] = React.useState('');
    const [error, setError] = React.useState(false);
    app.buyerInfo = Cookies.get('buyerInfo') ? JSON.parse(Cookies.get('buyerInfo')) : '';
    const handleRadioChange = event => {
        setValue(event.target.value);
        setError(false);
    };

    const handleAddAddress = event => {
        event.preventDefault();
        console.log(app);
        const formInstance = document.querySelector('#addAddressForm');
        const formData = new FormData(formInstance);
        const obj = {
            active: true,
            name: `${formData.get('firstName')} ${formData.get('lastName')}`,
            buyerid: app.buyerInfo.buyerid,
            flatNumber: formData.get('unit'),
            address: formData.get('address'),
            city: formData.get('city'),
            latitude: app.latitude,
            longitude: app.longitude,
            mobileNo: formData.get('mobileNo'),
            pincode: formData.get('zip'),
            state: formData.get('state'),
            toReach: formData.get('toReach')
        };
        AppConfigService.apiService.saveAddress(obj).then(res => {
            if (res.data.code === '200') {
                for (let index = 0; index < res.data.data.length; index++) {
                    if (index == 0) res.data.data[index].checked = true;
                    else res.data.data[index].checked = false;
                }
                setbuyerAddress(res.data.data);
                setValue(getCheckedAddress(res.data.data));
                sessionStorage.setItem('selectedAddress', JSON.stringify(res.data.data[0]));
            }
        });
    };
    const id = app.buyerInfo && app.buyerInfo.buyerid;
    const mobileNo = app.buyerInfo && app.buyerInfo.mobileNo;
    const requestParams = {
        id: id,
        mobileno: mobileNo
    };

    useEffect(() => {
        AppConfigService.apiService.getBuyerInfo(requestParams).then(res => {
            if (
                res.data.code == '200' &&
                res.data.data.buyerAddress &&
                res.data.data.buyerAddress.length > 0
            ) {
                for (let index = 0; index < res.data.data.buyerAddress.length; index++) {
                    if (index == 0) res.data.data.buyerAddress[index].checked = true;
                    else res.data.data.buyerAddress[index].checked = false;
                }
                setbuyerAddress(res.data.data.buyerAddress);
                setValue(getCheckedAddress(res.data.data.buyerAddress));
                sessionStorage.setItem(
                    'selectedAddress',
                    JSON.stringify(res.data.data.buyerAddress[0])
                );
            }
        });
    }, []);

    const handleSubmit = event => {
        event.preventDefault();
        app.selectedAddress = value;
        props.handleNext();
    };
    return (
        <React.Fragment>
            <form onSubmit={handleSubmit}>
                <FormControl component="fieldset" error={error} className={classes.formControl}>
                    <RadioGroup
                        aria-label="quiz"
                        name="quiz"
                        value={value}
                        onChange={handleRadioChange}
                    >
                        {buyerAddress.length > 0 &&
                            buyerAddress.map(address => {
                                return (
                                    <FormControlLabel
                                        key={address.addressid}
                                        value={address.addressid.toString()}
                                        control={<Radio />}
                                        label={address.address}
                                    />
                                );
                            })}
                    </RadioGroup>
                    <Button
                        type="submit"
                        variant="outlined"
                        color="primary"
                        className={classes.button}
                    >
                        Next
                    </Button>
                </FormControl>
            </form>
            <Divider variant="middle" />
            <Typography color="textSecondary" display="block" variant="caption" align="center">
                OR
            </Typography>
            <Typography variant="h6" align="center" gutterBottom>
                Add Shipping address
            </Typography>
            <form onSubmit={handleAddAddress} id="addAddressForm">
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="firstName"
                            name="firstName"
                            label="First name"
                            fullWidth
                            autoComplete="given-name"
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="lastName"
                            name="lastName"
                            label="Last name"
                            fullWidth
                            autoComplete="family-name"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="unit"
                            name="unit"
                            label="Flat, House No, Building, Company, Apartment"
                            fullWidth
                            autoComplete="shipping unit"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="address"
                            name="address"
                            label="Address line"
                            fullWidth
                            autoComplete="shipping address-line"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="toReach"
                            name="toReach"
                            label="How To Reach"
                            fullWidth
                            autoComplete="shipping toreach"
                        />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="city"
                            name="city"
                            label="City"
                            fullWidth
                            autoComplete="shipping address-level2"
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="state"
                            name="state"
                            label="State/Province/Region"
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="zip"
                            name="zip"
                            label="Zip / Postal code"
                            fullWidth
                            autoComplete="shipping postal-code"
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="country"
                            name="country"
                            label="Country"
                            fullWidth
                            autoComplete="shipping country"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="mobileNo"
                            name="mobileNo"
                            label="Mobile Number"
                            fullWidth
                            required
                            autoComplete="shipping mobileNo"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            type="submit"
                            variant="outlined"
                            color="primary"
                            className={classes.button}
                            fullWidth
                        >
                            Add Address
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </React.Fragment>
    );
}
