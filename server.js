//Importing dependencies
const express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();
const compression = require('compression');
var path = require('path');
const port = 3000;

//Starting Express app
const app = express();
app.use(compression());
//Set the base path to the angular-test dist folder
app.use((req, res, next) => {
    console.log('in use');
    console.log(next);
    if (req.originalUrl.indexOf('data=') >= 0) {
        var token = req.originalUrl.split('data=')[1];
        res.cookie('accessToken', token, {
            overwrite: true,
            encode: v => v
        });
    }
    next();
});
// for parsing application/json
app.use(bodyParser.json());

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true }));
//form-urlencoded

// for parsing multipart/form-data
app.use(upload.array());
app.use(express.static(path.join(__dirname, 'dist')));
//Any routes will be redirected to the angular app
app.get('/', function(req, res) {
    console.log('in /');
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

app.post('/home', function(req, res) {
    console.log('in home');
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});
app.post('/checkout', (req, res, next) => {
    console.log(req.body); // your JSON
    //response.send(req.body);
    res.cookie('response', req.body, {
        overwrite: true,
        encode: v => v
    });
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});
app.get('*', function(req, res) {
    console.log('in *');
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//Starting server on port 8081
app.listen(port, () => {
    console.log('Server started!');
    console.log('on port ', port);
});
function replaceAll(string, search, replace) {
    return string.split(search).join(replace);
}
