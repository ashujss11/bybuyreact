const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        host: 'localhost',
        hot: true,
        historyApiFallback: true,
        open: true,
        port:4040
    },

    plugins: [
        new BundleAnalyzerPlugin(),
    ],
});